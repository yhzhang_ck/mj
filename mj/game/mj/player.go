//player

package mj

type player struct {
	id   int
	info struct {
		hand []int
		out  []int
	}
}

func (p *player) send(msg []byte) {

}

func (p *player) recv(msg []byte) {

}

func (t *player) broadcast(msg []byte) {

}
