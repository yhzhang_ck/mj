package mj

import (
	//"encoding/json"
	"fmt"
	"log"
	"strconv"
)

type client struct {
	uid int
}

func cardsToString(cards []int) string {
	var str string
	colors := []string{"万", "条", "筒", "字"}
	for i, c := range cards {
		if c > 0 {
			value := i%9 + 1
			color := colors[i/9]
			for j := 0; j < c; j++ {
				str += strconv.Itoa(value) + color + " , "
			}
		}
	}
	str += "\n"
	return str

}

func showCards(cards []int) {
	str := cardsToString(cards)
	fmt.Println(str)
}

func Cli_send(msg interface{}) {

}

func Cli_recv(msg_head int, msg interface{}) {
	//var gs SUB_GAME_START
	//err := json.Unmarshal([]byte(msg), &gs)
	//if err != nil {
	//	fmt.Printf(err.Error())
	//}
	log.Printf("Recv msg -->: {%d ,%v}\n\n", msg_head, msg)
}

func Main_client() {
	cards := [34]int{
		3, 3, 3, 1, 0, 0, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0,
	}
	cardsToString(cards[:])
}
