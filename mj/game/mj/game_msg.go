package mj

const (
	START = iota + 1
	ADD
	EAT
	PENG
	OUT_GANG
	MING_GANG
	PENG_GANG
	OUT
	SELECT
	HU
	OVER
)

type SUB_GAME_START struct {
	Zhuang_id  int
	Curr_round int
      Next_idx   int
}
