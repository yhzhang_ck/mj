package mj

import (
	//"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	"github.com/looplab/fsm"
)

var (
	intSet   []byte = []byte(`0123456789`)
	charSet  []byte = []byte(`abcdefghijklnmopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUVWXYZ0123456789`)
	tableMgr        = make(map[string]*table) // key: group name
)

func init() {
	rand.Seed(time.Now().Unix())
}

type table struct {
	FSM *fsm.FSM

	idx        int
	id         string
	name       string
	cards      []int
	member     [4]player
	can_out    bool
	liuju      bool
	zhuang_idx int
	round      int

	muMember sync.RWMutex
	msgs     chan []byte
}

//工厂方法创建房间
func Newtable() *table {
	t := &table{}

	t.FSM = fsm.NewFSM(
		"wait",
		fsm.Events{
			{Name: "wait", Src: []string{"gameover"}, Dst: "wait"},
			{Name: "gamestart", Src: []string{"wait"}, Dst: "gamestart"},
			{Name: "gameover", Src: []string{"gamestart"}, Dst: "gameover"},
		},
		fsm.Callbacks{
			"wait": func(e *fsm.Event) {
				fmt.Println("after_scan: " + e.FSM.Current())
			},
			"gamestart": func(e *fsm.Event) {
				//fmt.Println("working: " + e.FSM.Current())
				t.GameStart()
			},
			"gameover": func(e *fsm.Event) {
				log.Println("situation: " + e.FSM.Current())
				t.GameOver()
			},
		},
	)

	return t
}

func genRandName(size int) string {
	r := make([]byte, size)
	for i := 0; i < size; i++ {
		r[i] = charSet[rand.Intn(len(charSet))]
	}
	return string(r)
}

func genRandNum(size int) string {
	r := make([]byte, size)
	for i := 0; i < size; i++ {
		r[i] = intSet[rand.Intn(len(charSet))]
	}
	return string(r)
}

func (t *table) nextIdx() int {
	return (t.idx + 1) % 4
}

func (t *table) Sendto(pid int, msg_head int, msg interface{}) {
	Cli_recv(msg_head, msg)
}

//发送消息
func (t *table) BroadCast(msg_head int, msg interface{}) {
	for i := 0; i < 4; i++ {
		t.Sendto(i, msg_head, msg)
	}
}

//等待用户登陆并且开启一个房间
func (t *table) WaitStart() {

}

//房间游戏开始
func (t *table) GameStart() {
	fmt.Println("----> game start 000")
	Start_room(t)
}

//房间游戏结束
func (r *table) GameOver() {

}

func Main_table() {
	t := Newtable()

	err := t.FSM.Event("gamestart")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("left cards : %3v\n", t.cards)
	for i := 0; i < 4; i++ {
		fmt.Printf("player hands : %3d \n", t.member[i].info.hand)
	}
}
