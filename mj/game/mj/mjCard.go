//常用函数

package mj

import (
	"math/rand"
	"time"
)

const (
	WikNull   = 0x00
	WikLeft   = 0x01
	WikCenter = 0x10
	WikRight  = 0x100
	WikPeng   = 0x200
)

var (
	gui_index = 34
)

func cardColor(card int) int {
	return card / 9
}

func cardValue(card int) int {
	return card % 9
}

func setGuiIndex(idx int) {
	gui_index = idx
}

func guiIndex() int {
	return gui_index
}

//洗牌
func Shuffle(cards []int) bool {
	rand.Seed(int64(time.Now().Nanosecond()))
	idx := 0
	n := len(cards)
	for i := 0; i < n; i++ {
		idx = rand.Intn(n - i)
		cards[idx], cards[n-i-1] = cards[n-i-1], cards[idx]
	}
	return true
}

//配牌
func config(cards, tmpCards []int) bool {
	return true
}

func removeCards(cards []int, card int, count int) bool {
	num := cards[card]
	if num <= 0 || num <= count {
		return false
	}
	cards[card] = 0
	return true
}

func cardsCount(cards []int) int {
	sum := 0
	for _, c := range cards {
		sum += c
	}
	return sum
}

//判断吃
func checkEat(cards []int, card int) int {

	tmp := make([]int,len(cards))
	copy(tmp,cards)

	first := 0
	eatMask := 0
	eatMaskKind := [3]int{WikLeft, WikCenter, WikRight}
	for i := 0; i < 3; i++ {
		value := card % 9
		if (value >= i) && (value-i <= 6) {
			first = card - i
			if (gui_index != 34) && (gui_index >= first) && (gui_index <= first+2) {
				continue
			}
			if (card != first) && (tmp[first] == 0) {
				continue
			}
			if (card != first+1) && (tmp[first+1] == 0) {
				continue
			}
			if (card != first+2) && (tmp[first+2] == 0) {
				continue
			}
			eatMask |= eatMaskKind[i]
		}
	}
	return eatMask
}

func checkPeng(cards []int, card int) int {

	tmp := make([]int,len(cards))
	copy(tmp,cards)

	if tmp[card] >= 2 {
		return WikPeng
	} else {
		return WikNull
	}
}

func Is_13_19(cards []int, gui_index int) bool {

	tmp := make([]int,len(cards))
	copy(tmp,cards)

	gui_num := 0
	if gui_index != 34 {
		gui_num = tmp[gui_index]
		tmp[gui_index] = 0
	}

	sum := 0
	need := 0
	eye := false
	_SHISANYAO_CARDS := []int{0, 8, 9, 17, 18, 26, 27, 28, 29, 30, 31, 32, 33}
	for _, i := range _SHISANYAO_CARDS {
		c := tmp[i]
		if c != 0 {
			if c > 2 {
				return false
			} else if c == 2 {
				if eye {
					return false
				} else {
					eye = true
				}
			}
			sum = sum + c
		} else {
			need++
		}
	}

	if eye {
		return (need == gui_num) && (sum+gui_num == 14)
	} else {
		return (need+1 == gui_num) && (sum+gui_num == 14)
	}
}

func xiaoQiDui(cards []int, gui_index int) (bool, int) {

	tmp := make([]int,len(cards))
	copy(tmp,cards)

	gui_num := 0
	if gui_index != 34 {
		gui_num = tmp[gui_index]
		tmp[gui_index] = 0
	}

	sum := 0
	dui := 0
	need := 0
	lave := gui_num
	haohua := 0

	for i := 0; i < 34; i++ {
		c := tmp[i]
		if c != 0 {
			sum += c
			if c <= 2 {
				dui = dui + 1
				lave = lave - (2 - c)
				need = need + (2 - c)
			} else {
				haohua = haohua + 1
				lave = lave - (4 - c)
				need = need + (4 - c)
			}
		}
	}

	if lave/2 >= dui && lave >= 0 {
		haohua = haohua + dui + (lave/2-dui)/2
	} else if lave/2 < dui && lave >= 0 {
		haohua = haohua + lave/2
	}

	return (sum+gui_num == 14) && (gui_num >= need), haohua
}

func pengPengHu(cards []int, gui_index int) bool {

	tmp := make([]int,len(cards))
	copy(tmp,cards)

      need := 0
      gui_num := 0
	if gui_index != 34 {
		gui_num = tmp[gui_index]
		tmp[gui_index] = 0
	}

	for i := 0; i < 34; i++ {
		c := tmp[i]
		if c > 0 && c < 4 {
			need = need + 3 - c
		} else if c == 4 {
			need = need + 2
		}
	}
	if (gui_num-need-2 >= 0) || (gui_num-need+1 >= 0) {
		return ((gui_num-need-2)%3 == 0 || (gui_num-need+1)%3 == 0)
	}
	return false
}

func cardsColors(cards []int, gui_index int) []int {

	tmp := make([]int,len(cards))
	copy(tmp,cards)

      colors := []int{0, 0, 0, 0}
	if gui_index != 34 {
		tmp[gui_index] = 0
	}
	for i := 0; i < 34; i++ {
		if tmp[i] > 0 {
			color := i / 9
			colors[color] = 1
		}
	}
	return colors
}

func qingYiSe(cards []int, gui_index int) bool {
	colors := cardsColors(cards, gui_index)
	return ((colors[0]+colors[1]+colors[2] == 1) && colors[3] == 0)
}

func hunYiSe(cards []int, gui_index int) bool {
	colors := cardsColors(cards, gui_index)
	return ((colors[0]+colors[1]+colors[2] == 1) && colors[3] == 1)
}

func ziYiSe(cards []int, gui_index int) bool {
	colors := cardsColors(cards, gui_index)
	return ((colors[0]+colors[1]+colors[2] == 0) && colors[3] == 1)
}

func canHu(cards []int, gui_index int) bool {
	return Check_hu(cards,gui_index)
}

func isTing(cards []int, gui_index int) bool {
	tmp := make([]int, len(cards))

	for i := 0; i < 34; i++ {
		copy(tmp,cards[:])
		tmp[i] = tmp[i] + 1
		if Check_hu(tmp,gui_index) {
			return true
		}
	}
	return false
}

// 传入一副牌，返回所有的听牌
func tingCards(cards []int, gui_index int) []int {
	tmp := make([]int, len(cards)+1)
	rts := make([]int, 0)
	for i := 0; i < 34; i++ {
		copy(tmp,cards[:])
		tmp[i] = tmp[i] + 1
		if Check_hu(tmp,gui_index) {
			rts = append(rts, i)
		}
	}
	return rts
}

func quickSort(values []int, left int, right int) {
	if left < right {
		temp := values[left]
		i, j := left, right
		for {
			for values[j] >= temp && i < j {
				j--
			}
			for values[i] <= temp && i < j {
				i++
			}

			if i >= j {
				break
			}

			values[i], values[j] = values[j], values[i]
		}

		values[left] = values[i]
		values[i] = temp

		quickSort(values, left, i-1)
		quickSort(values, i+1, right)
	}
}
