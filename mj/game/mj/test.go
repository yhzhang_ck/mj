package mj

import (
	"fmt"
)

func hu_test() {
	/*cards:=[34]int{
	      1,0,0,0,0,0,0,0,1,
	      1,0,0,2,0,0,0,0,1,
	      1,0,0,0,0,0,0,0,1,
	      1,1,1,1,1,1,0,
	}*/
	cards := [34]int{
		3, 3, 3, 1, 0, 0, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0,
	}
	gui_index := 13
	xiaoQiDui, haohua := xiaoQiDui(cards[:], gui_index)
	fmt.Println("hu         : ", Check_hu(cards[:], gui_index))
	fmt.Println("Is_13_19   : ", Is_13_19(cards[:], gui_index))
	fmt.Println("xiaoQiDui  : ", xiaoQiDui, " , haohua : ", haohua)
	fmt.Println("pengPengHu : ", pengPengHu(cards[:], gui_index))
	fmt.Println("qingYiSe   : ", qingYiSe(cards[:], gui_index))
}

func Main_test() {
	hu_test()
}
